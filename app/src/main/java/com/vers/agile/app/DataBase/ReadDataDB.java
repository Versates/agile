package com.vers.agile.app.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by vers on 31.10.14.
 */
public class ReadDataDB {

    private Context context;

    public ReadDataDB(Context context) {
        this.context = context;
    }

    public void readLector() {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();


        String query = "SELECT * FROM lector";
        Cursor cursor2 = sqLiteDatabase.rawQuery(query, null);

        while (cursor2.moveToNext()) {
            String F_Name = cursor2.getString(cursor2.getColumnIndex("F_Name"));
            String L_Name = cursor2.getString(cursor2.getColumnIndex("L_Name"));
            String Email = cursor2.getString(cursor2.getColumnIndex("Email"));
            System.out.println("ROW HAS NAME: " + F_Name + " " + L_Name + " " + Email);
        }
        cursor2.close();

        sqLiteDatabase.close();
        dataBaseCon.close();

        System.out.println("gbgbg");

    }

    public void readLection() {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();

        String query = "SELECT * FROM lection";
        Cursor cursor2 = sqLiteDatabase.rawQuery(query, null);

        while (cursor2.moveToNext()) {
            String Theme = cursor2.getString(cursor2.getColumnIndex("Theme"));
            String Lection_text = cursor2.getString(cursor2.getColumnIndex("Lection_text"));
            String Place = cursor2.getString(cursor2.getColumnIndex("Place"));
            String Date = cursor2.getString(cursor2.getColumnIndex("Date"));
            String Time = cursor2.getString(cursor2.getColumnIndex("Time"));
            int ID_Lector = cursor2.getInt(cursor2.getColumnIndex("ID_Lector"));
            System.out.println("ROW HAS NAME: " + Theme + " " + Lection_text + " " + Place + " " + Date + " " + Time + " " + ID_Lector);
        }

        cursor2.close();
        dataBaseCon.close();
        System.out.println("dfdfdf");

    }

    public void readUser() {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();

        String query = "SELECT * FROM user";
        Cursor cursor2 = sqLiteDatabase.rawQuery(query, null);

        while (cursor2.moveToNext()) {
            String F_Name = cursor2.getString(cursor2.getColumnIndex("F_Name"));
            String L_Name = cursor2.getString(cursor2.getColumnIndex("L_Name"));
            String Email = cursor2.getString(cursor2.getColumnIndex("Email"));
            String Login = cursor2.getString(cursor2.getColumnIndex("Login"));
            String Password = cursor2.getString(cursor2.getColumnIndex("Password"));
            System.out.println("ROW HAS NAME: " + F_Name + " " + L_Name + " " + Email + " " + Login + " " + Password);
        }

        cursor2.close();
        sqLiteDatabase.close();
        dataBaseCon.close();
    }

    public void readList() {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();

        String query = "SELECT * FROM list";
        Cursor cursor2 = sqLiteDatabase.rawQuery(query, null);

        while (cursor2.moveToNext()) {
            String State = cursor2.getString(cursor2.getColumnIndex("State"));
            int ID_Lection = cursor2.getInt(cursor2.getColumnIndex("ID_Lection"));
            int ID_User = cursor2.getInt(cursor2.getColumnIndex("ID_User"));
            System.out.println("ROW HAS NAME: " + State + " " + ID_Lection + " " + ID_User);
        }

        cursor2.close();
        sqLiteDatabase.close();
        dataBaseCon.close();
        System.out.println("okokokk");
    }

}
