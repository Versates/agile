package com.vers.agile.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import com.vers.agile.app.DataBase.ReadDataDB;
import com.vers.agile.app.DataBase.WriteDataDB;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

//        DataBaseCon dataBaseCon = new DataBaseCon(this);
//        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();
//
//        sqLiteDatabase.close();
//        dataBaseCon.close();
    }

    public void onCl2(View view) {

        WriteDataDB writeDataDB = new WriteDataDB(this);
        writeDataDB.writeLector(2);
        writeDataDB.writeLection(3);
        writeDataDB.writeUser(4);
        writeDataDB.writeList(5);

        ReadDataDB readDataDB = new ReadDataDB(this);
        readDataDB.readLector();
        readDataDB.readLection();
        readDataDB.readUser();
        readDataDB.readList();

//        Intent intent = new Intent(this, List2.class);
//        startActivity(intent);
    }

}