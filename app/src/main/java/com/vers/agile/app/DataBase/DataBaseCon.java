package com.vers.agile.app.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DataBaseCon extends SQLiteOpenHelper implements BaseColumns {
    private static final String DATABASE_NAME = "user_database.db";
    private static final int DATABASE_VERSION = 16;


    private static final String SQL_CREATE_LECTOR = "CREATE TABLE lector ("
            + DataBaseCon._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "F_Name VARCHAR(255), " +
            "L_Name VARCHAR(255), " +
            "Email VARCHAR(255) " +
            ");";

    private static final String SQL_CREATE_LECTION = "CREATE TABLE lection ("
            + DataBaseCon._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "Theme VARCHAR(255), " +
            "Lection_text TEXT, " +
            "Place VARCHAR(255), " +
            "Date VARCHAR(255), " +
            "Time VARCHAR(255), " +
            "ID_Lector INTEGER" +
            ");";

    private static final String SQL_CREATE_USER = "CREATE TABLE user ("
            + DataBaseCon._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "F_Name VARCHAR(255), " +
            "L_Name VARCHAR(255), " +
            "Email VARCHAR(255), " +
            "Login VARCHAR(255), " +
            "Password VARCHAR(255) " +
            ");";

    private static final String SQL_CREATE_LIST = "CREATE TABLE list ("
            + DataBaseCon._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "State VARCHAR(255), " +
            "ID_Lection INTEGER, " +
            "ID_User INTEGER, " +
            "FOREIGN KEY(ID_Lection) REFERENCES lection(id), " +
            "FOREIGN KEY(ID_User) REFERENCES user(id)" +
            ");";

    private static final String SQL_DELETE_LECTOR = "DROP TABLE IF EXISTS lector;";
    private static final String SQL_DELETE_LECTION = "DROP TABLE IF EXISTS lection;";
    private static final String SQL_DELETE_USER = "DROP TABLE IF EXISTS user;";
    private static final String SQL_DELETE_LIST = "DROP TABLE IF EXISTS list;";

    public DataBaseCon(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_LECTOR);
        sqLiteDatabase.execSQL(SQL_CREATE_LECTION);
        sqLiteDatabase.execSQL(SQL_CREATE_USER);
        sqLiteDatabase.execSQL(SQL_CREATE_LIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.w("LOG_TAG", "Обновление базы данных с версии " + oldVersion
                + " до версии " + newVersion + ", которое удалит все старые данные");
        // Удаляем предыдущую таблицу при апгрейде
        sqLiteDatabase.execSQL(SQL_DELETE_LECTOR);
        sqLiteDatabase.execSQL(SQL_DELETE_LECTION);
        sqLiteDatabase.execSQL(SQL_DELETE_USER);
        sqLiteDatabase.execSQL(SQL_DELETE_LIST);
        // Создаём новый экземпляр таблицы
        onCreate(sqLiteDatabase);
    }

    public void fillDB() {

    }
}
