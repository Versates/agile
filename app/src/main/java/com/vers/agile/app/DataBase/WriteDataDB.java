package com.vers.agile.app.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;


public class WriteDataDB {
    private Context context;

    public WriteDataDB(Context context) {
        this.context = context;
    }

    public void writeLector(int i) {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();

        String insertQuery;
        insertQuery = "INSERT INTO lector (F_Name, L_Name, Email) VALUES " +
                "('Иван', 'Иванов', 'ivanov@yandex.ru');";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO lector (F_Name, L_Name, Email) VALUES " +
                "('Александр', 'Основ', 'osnov@yandex.ru');";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO lector (F_Name, L_Name, Email) VALUES " +
                "('Анатолий', 'Наумов', 'naumov@yandex.ru');";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO lector (F_Name, L_Name, Email) VALUES " +
                "('Руслан', 'Антонов', 'antonov@yandex.ru');";
        sqLiteDatabase.execSQL(insertQuery);

        sqLiteDatabase.close();
        dataBaseCon.close();
        System.out.println("Lector add correctly");

    }

    public void writeLection(int i) {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();

        String insertQuery;
        insertQuery = "INSERT INTO lection (Theme, Lection_text, Place, Date, Time, ID_Lector) VALUES " +
                "('Тема1', 'Доклад1', 'ауд.25', '20.11.2014', '14:00', 1);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO lection (Theme, Lection_text, Place, Date, Time, ID_Lector) VALUES " +
                "('Тема1', 'Доклад2', 'ауд.27', '22.11.2014', '16:00', 2);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO lection (Theme, Lection_text, Place, Date, Time, ID_Lector) VALUES " +
                "('Тема1', 'Доклад3', 'ауд.29', '20.11.2014', '12:00', 3);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO lection (Theme, Lection_text, Place, Date, Time, ID_Lector) VALUES " +
                "('Тема1', 'Доклад4', 'ауд.26', '21.11.2014', '13:30', 4);";
        sqLiteDatabase.execSQL(insertQuery);

        sqLiteDatabase.close();
        dataBaseCon.close();
        System.out.println("Lection add correctly");

    }

    public void writeUser(int i) {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();

        String insertQuery;
        insertQuery = "INSERT INTO user (F_Name, L_Name, Email, Login, Password) VALUES " +
                "('Николай', 'Лопух', 'nikolay@gmail.com', 'qwer', '1234');";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO user (F_Name, L_Name, Email, Login, Password) VALUES " +
                "('Александр', 'Бойко', 'bojko@gmail.com', 'asdf', '2345');";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO user (F_Name, L_Name, Email, Login, Password) VALUES " +
                "('Роман', 'Дубовик', 'dybovik@gmail.com', 'zxcv', '3456');";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO user (F_Name, L_Name, Email, Login, Password) VALUES " +
                "('Владимир', 'Крыжанов', 'vladimir@gmail.com', 'tgbn', '4567');";
        sqLiteDatabase.execSQL(insertQuery);

        sqLiteDatabase.close();
        dataBaseCon.close();
        System.out.println("Users add correctly");

    }

    public void writeList(int i) {
        DataBaseCon dataBaseCon = new DataBaseCon(context);
        SQLiteDatabase sqLiteDatabase = dataBaseCon.getWritableDatabase();
        String insertQuery;

        insertQuery = "INSERT INTO list (State, ID_Lection, ID_User) VALUES " +
                "('true', 1, 1);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO list (State, ID_Lection, ID_User) VALUES " +
                "('true', 2, 1);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO list (State, ID_Lection, ID_User) VALUES " +
                "('true', 1, 2);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO list (State, ID_Lection, ID_User) VALUES " +
                "('true', 2, 3);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO list (State, ID_Lection, ID_User) VALUES " +
                "('true', 3, 3);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO list (State, ID_Lection, ID_User) VALUES " +
                "('true', 4, 3);";
        sqLiteDatabase.execSQL(insertQuery);
        insertQuery = "INSERT INTO list (State, ID_Lection, ID_User) VALUES " +
                "('true', 1, 4);";
        sqLiteDatabase.execSQL(insertQuery);

        sqLiteDatabase.close();
        dataBaseCon.close();
        System.out.println("List add correctly");
    }


}